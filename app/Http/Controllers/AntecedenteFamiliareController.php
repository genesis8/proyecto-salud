<?php

namespace App\Http\Controllers;

use App\AntecedenteFamiliare;
use Illuminate\Http\Request;

class AntecedenteFamiliareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AntecedenteFamiliare  $antecedenteFamiliare
     * @return \Illuminate\Http\Response
     */
    public function show(AntecedenteFamiliare $antecedenteFamiliare)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AntecedenteFamiliare  $antecedenteFamiliare
     * @return \Illuminate\Http\Response
     */
    public function edit(AntecedenteFamiliare $antecedenteFamiliare)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AntecedenteFamiliare  $antecedenteFamiliare
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AntecedenteFamiliare $antecedenteFamiliare)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AntecedenteFamiliare  $antecedenteFamiliare
     * @return \Illuminate\Http\Response
     */
    public function destroy(AntecedenteFamiliare $antecedenteFamiliare)
    {
        //
    }
}
