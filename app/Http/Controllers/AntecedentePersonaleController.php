<?php

namespace App\Http\Controllers;

use App\AntecedentePersonale;
use Illuminate\Http\Request;

class AntecedentePersonaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AntecedentePersonale  $antecedentePersonale
     * @return \Illuminate\Http\Response
     */
    public function show(AntecedentePersonale $antecedentePersonale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AntecedentePersonale  $antecedentePersonale
     * @return \Illuminate\Http\Response
     */
    public function edit(AntecedentePersonale $antecedentePersonale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AntecedentePersonale  $antecedentePersonale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AntecedentePersonale $antecedentePersonale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AntecedentePersonale  $antecedentePersonale
     * @return \Illuminate\Http\Response
     */
    public function destroy(AntecedentePersonale $antecedentePersonale)
    {
        //
    }
}
