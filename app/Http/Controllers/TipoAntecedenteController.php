<?php

namespace App\Http\Controllers;

use App\TipoAntecedente;
use Illuminate\Http\Request;

class TipoAntecedenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoAntecedente  $tipoAntecedente
     * @return \Illuminate\Http\Response
     */
    public function show(TipoAntecedente $tipoAntecedente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoAntecedente  $tipoAntecedente
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoAntecedente $tipoAntecedente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoAntecedente  $tipoAntecedente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoAntecedente $tipoAntecedente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoAntecedente  $tipoAntecedente
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoAntecedente $tipoAntecedente)
    {
        //
    }
}
