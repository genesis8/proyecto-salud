<?php

namespace App\Http\Controllers;

use App\AntecedenteMedicoInfancia;
use Illuminate\Http\Request;

class AntecedenteMedicoInfanciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AntecedenteMedicoInfancia  $antecedenteMedicoInfancia
     * @return \Illuminate\Http\Response
     */
    public function show(AntecedenteMedicoInfancia $antecedenteMedicoInfancia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AntecedenteMedicoInfancia  $antecedenteMedicoInfancia
     * @return \Illuminate\Http\Response
     */
    public function edit(AntecedenteMedicoInfancia $antecedenteMedicoInfancia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AntecedenteMedicoInfancia  $antecedenteMedicoInfancia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AntecedenteMedicoInfancia $antecedenteMedicoInfancia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AntecedenteMedicoInfancia  $antecedenteMedicoInfancia
     * @return \Illuminate\Http\Response
     */
    public function destroy(AntecedenteMedicoInfancia $antecedenteMedicoInfancia)
    {
        //
    }
}
